#include "socket.h"

Socket::Socket()
{

}

Socket::Socket(QHostAddress addr, int port)
{
    server = new QTcpServer();
    server->listen(addr, port);

    connect(server, SIGNAL(newConnection()), this, SLOT(newConnectionServer()));
}

Socket::~Socket()
{
    socket->close();
    socket->disconnectFromHost();
    socket->deleteLater();
    server->close();
    server->disconnect();
    server->deleteLater();
}

void Socket::newConnectionServer()
{
    socket = new QTcpSocket();
    socket = server->nextPendingConnection();

    connect(socket, SIGNAL(readyRead()), this, SLOT(readSocket()));
    connect(server, SIGNAL(disconnected), socket, SLOT(disconnectSocket()));

    qDebug() << "Socket Connected";
}

void Socket::disconnectSocket()
{
    qDebug() << "Socket was disconnected";

    server->deleteLater();
}

void Socket::writeOnSocket(QByteArray value)
{
    if(!socket) {
        qWarning() << "No socket connection";
        return;
    }
    qDebug() << "QT: Writing data on socket";
    socket->write(value);
}

void Socket::writeOnSocketWithResponse(QByteArray value)
{
    if(!socket) {
        qWarning() << "No socket connection";
        return;
    }

    qDebug() << "QT: Writing data on socket";
    socket->write(value);
    waitingResponse = true;

    if(socket->waitForBytesWritten(3000)) {

        if(socket->waitForReadyRead(5000)) {
            dataReceived = socket->readAll();
            qDebug() << "Socket received: " << dataReceived;
        } else {
            qDebug() << "Nothing was received from socket";
        }
    }

    waitingResponse = false;
}

void Socket::readSocket()
{
    /*
     *  Significa que está aguardando chegar
     *  uma mensagem na função writeOnSocketWithResponse
     * Portanto, ignora os dados que chegaram no slot
     */
    if(waitingResponse)
        return;

    dataReceived = socket->readAll();

    qDebug() << "Socket received: " << dataReceived;
}


