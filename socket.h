#ifndef SOCKET_H
#define SOCKET_H

/*
 * Libs
*/
#include <QTcpServer>
#include <QTcpSocket>
#include <QObject>
#include <QHostAddress>


class Socket : public QObject
{
    Q_OBJECT
public:
    Socket();
    Socket(QHostAddress addr, int port);
    ~Socket();

signals:

public slots:
    void readSocket();
    void newConnectionServer();
    void disconnectSocket();

public:
    void writeOnSocket(QByteArray value);
    void writeOnSocketWithResponse(QByteArray value);

private:
    QTcpServer *server;
    QTcpSocket *socket;

    QByteArray dataReceived;

    bool waitingResponse;

};

#endif // SOCKET_H
