#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QHostAddress>
#include <QTest>
#include <QString>
#include <socket.h>

int main(int argc, char *argv[])
{
    QGuiApplication app(argc, argv);

    QQmlApplicationEngine engine;
    engine.load(QUrl(QStringLiteral("qrc:/main.qml")));
    if (engine.rootObjects().isEmpty())
        return -1;

    Socket *socket = new Socket(QHostAddress("127.0.0.1"), 9000);
    QTest::qWait(3000);

    if(argc > 1) {

        if(QString::compare(argv[1], "w", Qt::CaseInsensitive) == 0) {
            socket->writeOnSocket(argv[2]);
        } else if(QString::compare(argv[1], "wr", Qt::CaseInsensitive) == 0) {
            socket->writeOnSocketWithResponse(argv[2]);
        }
    } else {
        socket->writeOnSocketWithResponse("Test Socket from QT");
    }


    return app.exec();
}
